#!/usr/bin/env perl

use strict;
use warnings;

foreach my $item_file (@ARGV) {    
    my $input_file = "filter.txt";
    my $out_file = "tmp-file.txt";
    my $item_number_position = 0;
      
    open(my $IN_FILE, '<', $input_file) or die "Couldn't open '$input_file' $!";
    while (my $item = <$IN_FILE>) {
        chomp($item);
        open (my $ITEM_FILE, '<', $item_file) or die "Couldn't open '$item_file' $!";
        print "Begin Processing $item_file for $item\n";
        open (my $OUT_FILE, '>', $out_file) or die "Couldn't open '$out_file' $!";
        while (my $item_line = <$ITEM_FILE>) {
            if (index($item_line, $item, $item_number_position) == -1) {
                # add line to out file only if it doesn't have the item number
                print $OUT_FILE $item_line;
            }
        }
        close $OUT_FILE;
        close $ITEM_FILE;
        rename $out_file, $item_file; # overwrite item file with the out file
        print "Done Processing $item_file for $item\n";
    }
    close $IN_FILE;
 }
 