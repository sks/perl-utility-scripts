#!/usr/bin/env perl
use strict;
use warnings;

my ($fish_file, $sea_file) = @ARGV; # input files
my $out_file = 'out.tmp'; # temp file for processing

open FFH, '<', $fish_file or die "Couldn't open $fish_file";
my @fishes = <FFH>; # Load fishfile into @fishes array
close FFH;

# Loop through fishes for each fish
for my $fish (@fishes) {
    my ($item_number, $state) = split / /, $fish; # "<item> <state>" "1344 IL"
    # removes EOL; chomp does not have cross-platform support for EOL
    $state =~ s/\r?\n$//;

    open SFH, '<', $sea_file or die "Couldn't open $sea_file";
    open OFH, '>', $out_file or die "Couldn't open $out_file";

    # Look up $item_number in $sea_file and replace the $state
    while (my $line = <SFH>) {
        if ($line =~ m/$item_number/) {
          # Replace first two characters with state
          substr $line, 2, 2, $state;
          # $line =~ s/^\w{2}/$state/; # Regex here is overkill 
        }
        print OFH $line;
    }
    close SFH;
    close OFH;
    # Overwrite the sea file
    rename $out_file, $sea_file;
}

# LIMITATIONS
# Not sure if it handles large files